A simple, responsive, mobile-first personal website. Keep it cute, keep it clean 🙂

## Links
  [aponty.com](https://aponty.com).
  [www.aponty.com](https://www.aponty.com).

## About
  Felt like giving GitLab pages a shot. Lookin' good so far, thanks GitLab!

  Just a bit of good old vanilla HTML, CSS, JS.

  Resume current as of 01/2021.
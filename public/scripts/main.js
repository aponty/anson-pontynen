'use strict'
const buttons = document.querySelectorAll('.clickable');
const typeBubble = document.querySelector(".type_bubble");

let isScrolling;

const closeActive = (classString) => {
    const active = document.querySelectorAll(`.${classString}_content.active`);
    const highlight = document.querySelectorAll(`.${classString}.highlight`);

    active.forEach(node => node.classList.remove('active'));
    highlight.forEach(node => node.classList.remove('highlight'));
}

const handleTap = (button, e) => {
    if (event.cancelable) event.preventDefault();
    if (e.type === 'touchend' && isScrolling) return;

    const isActive = button.classList.contains('highlight');
    const isProjectHeader = button.classList.contains('project');

    const newProjectSelected = isProjectHeader && !isActive;
    const newLinkSelected = !isProjectHeader && !isActive;
    const closeThisLink = !isProjectHeader && isActive;

    if (newProjectSelected) closeActive('project');
    if (newLinkSelected) closeActive('link');
    if (closeThisLink) return closeActive('link');

    button.classList.add('highlight')
    button.nextElementSibling.classList.add('active');
}

buttons.forEach(button => {
    button.addEventListener('click', (e) => handleTap(button, e));
    button.addEventListener('touchend', (e) => handleTap(button, e));
})

document.body.addEventListener('touchmove', () => isScrolling = true);
// timeout delay makes scroll event override button's event handler
document.body.addEventListener('touchend', () => setTimeout(()=> isScrolling = false, 50));


const stringSplitter = string => new GraphemeSplitter().splitGraphemes(string);
  
const typewriter = new Typewriter(typeBubble, {
    loop: true,
    stringSplitter
});

typewriter
    .pauseFor(2500)
    .typeString("🖥️⌨️")
    .pauseFor(3000)
    .deleteAll(200)
    .pauseFor(2500)
    .typeString("⛰️🧗")
    .pauseFor(3000)
    .deleteAll(200)
    .pauseFor(2500)
    .typeString("👷🛠️")
    .pauseFor(3000)
    .deleteAll(200)
    .pauseFor(2500)
    .typeString("🏍️💨")
    .pauseFor(3000)
    .deleteAll(200)
    .pauseFor(2500)
    .typeString("📚👓")
    .pauseFor(3000)
    .deleteAll(200)
    .pauseFor(2500)
    .typeString("🗺️🚐")
    .pauseFor(3000)
    .deleteAll(200)
    .start()